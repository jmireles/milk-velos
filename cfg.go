package velos

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/url"
)

type Cfg struct {
	URL  string `json:"url"`
	User string `json:"user"`
	Pass string `json:"pass"`
	Log  int    `json:"log"`
}

func NewCfgFile(filepath string) (*Cfg, error) {
	if filepath == "" {
		// return no client for velos here and no error
		return nil, nil
	}
	var cfg Cfg
	if content, err := ioutil.ReadFile(filepath); err != nil {
		return nil, fmt.Errorf("Read file error: %s\n", err)
	
	} else if err := json.Unmarshal(content, &cfg); err != nil {
		return nil, fmt.Errorf("JSON error in file: %s %v", filepath, err)
	
	} else if err := cfg.Validate(); err != nil {
		return nil, err

	} else {
		return &cfg, nil
	}
}

func (c *Cfg) Validate() error {

	if _, err := url.Parse(c.URL); c.URL == "" || err != nil {
		return fmt.Errorf("Invalid URL: %v", err)
	}
	if c.User == "" {
		return fmt.Errorf("Invalid user")
	}
	if c.Pass == "" {
		return fmt.Errorf("Invalid pass")
	}
	return nil
}

func (c *Cfg) Auth() *Auth {
	return &Auth{
		user: c.User,
		pass: c.Pass,
	}
}

