package velos

import (
	"bytes"
	"fmt"
	"net/http"
)

type Client struct {
	cfg     *Cfg
	id      int
	session string
}

func NewClient(cfg *Cfg) *Client {
	return &Client{
		cfg: cfg,
		id:  1,
	}
}

func (c *Client) Login() ([]string, error) {
	return c.postApi(Session, Login, nil)
}

func (c *Client) StartMeasuring(start *Start) ([]string, error) {
	return c.startMeasuring(start.place, start.reattach)
}

func (c *Client) StopMeasuring(stop *Stop) ([]string, error) {
	return c.stopMeasuring(stop.place, stop.reason)
}

func (c *Client) startMeasuring(place int, reattach bool) ([]string, error) {
	if c.session == "" {
		c.Login()
	}
	return c.postApi(Smartflow, StartMeasuring, []any{ 
		place,
		reattach,
	})
}

func (c *Client) stopMeasuring(place int, reason TakeoffReason) ([]string, error) {
	if c.session == "" {
		c.Login()
	}
	return c.postApi(Smartflow, StopMeasuring, []any{ 
		place,
		reason,
	})
}

func (c *Client) GetVersion() ([]string, error) {
	if c.session == "" {
		c.Login()
	}
	// works:
	// [https://{SITE}/api/processunit
	// {"jsonrpc":"2.0","id":2,"method":"get_version","params":[]}
	// {"jsonrpc":"2.0","id":2,"result":"5.130.6.466"}]	
	return c.postApi(ProcessUnit, GetVersion, []any{
		map[string]any{
			"name":       "vpu_1", // seems is optional
			"ob_type":    "HostName",
			"ob_version": 1,
		},
	})
}

func (c *Client) postApi(api Api, method Method, params any) ([]string, error) {

	req := NewJsonRpcReq(c.id, method, params)

	url := fmt.Sprintf("%s%s", c.cfg.URL, api.String())
	result := []string{
		url,
	}

	reqBytes, err := req.Bytes()
	if err != nil {
		return nil, fmt.Errorf("req json err:%v", err)
	}
	result = append(result, req.String())

	post, err := http.NewRequest("POST", url, bytes.NewBuffer(reqBytes))
	if err != nil {
		return result, fmt.Errorf("post error:%v", err)
	}
	
	post.Header.Add("Content-Type", "application/json-rpc")
	post.Header.Add("Accept", "application/json-rpc")

//fmt.Println("Client.postApi session", c.session)
	if c.session == "" {
		post.SetBasicAuth(c.cfg.User, c.cfg.Pass)
	} else {
		post.Header.Add("Cookie", c.session)
	}

	client := &http.Client{}
	r, err := client.Do(post)
	if err != nil {
		return result, err
	}
	if session, ok := r.Header["Set-Cookie"]; ok {
		c.session = session[0]
	}
	switch r.StatusCode {

	case http.StatusOK:
		resp, err := NewResp(r.Body)
		if err != nil {
			return result, fmt.Errorf("resp error:%v", err)
		}
		if respId := int(resp.Id); c.id != respId {
			return result, fmt.Errorf("Different: c.Id=%d resp.Id=%d", c.id, respId)
		}
		c.id++
		result = append(result, resp.String())
		return result, nil

	case http.StatusBadRequest: // 400
		if resp, err := NewResp(r.Body); err == nil {
			result = append(result, resp.String())
		}
		return result, fmt.Errorf("400. Bad request: %s", url)

	case http.StatusUnauthorized: // 401
		if resp, err := NewResp(r.Body); err == nil {
			result = append(result, resp.String())
		}
		return result, fmt.Errorf("401. Unauthorized: %s", url)

	case http.StatusNotFound: // 404	
		if resp, err := NewResp(r.Body); err == nil {
			result = append(result, resp.String())
		}
		return result, fmt.Errorf("404. Not found: %s", url)
	
	default: // 405?
		if resp, err := NewResp(r.Body); err == nil {
			result = append(result, resp.String())
		}
		return result, fmt.Errorf("%d. response", r.StatusCode)	
	}
}

func (c *Client) Logout() {
	// TODO
}
