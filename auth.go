package velos

import (
	"fmt"
	"net/http"
)

type Auth struct {
	user string
	pass string
}

func NewAuth(user, pass string) *Auth {
	return &Auth{
		user: user,
		pass: pass,
	}
}

func (a *Auth) LoginOk(w http.ResponseWriter, r *http.Request) bool {
	user, pass, ok := r.BasicAuth()

	if a.user == user && a.pass == pass && ok {
		// Login success
		session := a.newSession()
		w.Header().Add("Set-Cookie", session)
		w.WriteHeader(200)
		return true
	} else {
		// Login error
		w.Header().Add("Set-Cookie", "") // clear session
		w.WriteHeader(401)
		return false
	}
}

func (a *Auth) CheckSession(w http.ResponseWriter, r *http.Request) error {

	if session := r.Header.Get("Cookie"); a.validSession(session) {
		w.WriteHeader(200)
		return nil
	} else {
		// return auth-error
		w.Header().Add("Set-Cookie", "") // clear session
		w.WriteHeader(401)
		err := fmt.Errorf("Invalid session:[%s]", session)
		return err
	}
}

func (a *Auth) newSession() string {
	return "123456789" // FIXED
}

func (a *Auth) validSession(session string) bool {
	for _, current := range []string{
		"123456789",
	} {
		if current == session {
			return true
		}
	}
	return false
}
