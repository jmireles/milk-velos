package velos

import (
	"fmt"
)

type Api string

const (
	Devices     Api = "devices"
	Livestock   Api = "livestock"
	Milk        Api = "milk"
	Session     Api = "session"
	Separation  Api = "separation"
	Smartflow   Api = "smartflow"
	Positioning Api = "positioning"
	ProcessUnit Api = "processunit"
	Weighing    Api = "weighing"
)

func (a Api) String() string {
	return fmt.Sprintf("/api/%s", string(a))
}

type Method string

const (
	Login          Method = "login"
	Logout         Method = "logout"
	
	StartMeasuring Method = "start_measuring"
	StopMeasuring  Method = "stop_measuring"
	
	Clear                     Method = "clear"
	GetVersion                Method = "get_version"
	GetLastMilking            Method = "get_last_milking"
	GetMilkData               Method = "get_milk_data"
	GetMilkings               Method = "get_milkings"
	GetMilkingsForAnimal      Method = "get_milkings_for_animal"
	GetMilkingsForAnimalSince Method = "get_milkings_for_animal_since"

	GetLogicalAddresses       Method = "get_logical_addresses"
	GetAnimal                 Method = "get_animal"

	GetDailyAnimalWeightSummary Method = "get_daily_animal_weight_summary"

)




