package velos

import (
	"fmt"
	"context"
	"io/ioutil"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
	"testing"
	"time"

	"gitlab.com/jmireles/cans-base"
)

/*
cd milk_velos
git init
git remote add origin https://gitlab.com/jmireles/milk-velos.git

set files, do tests...

git add .
git commit -m "Initial commit"
git branch -m master main
git push -u origin main
*/

var (
	srv1 = &Auth{
		user: "userName",
		pass: "password",
	}
	srv2 = &Auth{
		user: "username",
		pass: "password",
	}
	cli1 = &Cfg{
		URL:  "http://localhost:8080",
		User: srv1.user,
		Pass: srv2.pass,
	}
	cli2 = &Cfg{
		URL:  "http://localhost:8080",
		User: srv2.user,
		Pass: srv2.pass,
	}
)

func TestCfg(t *testing.T) {

	tt := base.Test{ T:t }
	f, err := ioutil.TempFile("", "velos.json")
	tt.Ok(err)
	defer syscall.Unlink(f.Name())
	
	// write
	ioutil.WriteFile(f.Name(), []byte(`{
		"url":  "http://localhost:8080",
		"user": "userName",
		"pass": "password"
	}`), 0777)
	// read
	cfg, err := NewCfgFile(f.Name())
	tt.Ok(err)
	tt.Equals("http://localhost:8080", cfg.URL)
	tt.Equals(srv1, cfg.Auth())
	t.Log(f.Name(), cfg)

	for _, bad := range []string {
		// wrong json
		``,
		// empty addr
		`{
			"user": "userName",
			"pass": "password"
		}`,
		// addr without port
		`{
			"url":"http://localhost"
		}`,
		// no user/pass
		`{
			"url":":"http://localhost:8080",
			"user":"",
			"pass":"",
		}`,
		// empty pass
		`{
			"url":"http://localhost:8080",
			"user": "us3r"
		}`,
		// empty user
		`{
			"url":"http://localhost:8080",
			"pass": "pa55"
		}`,
	} {
		f, err := ioutil.TempFile("", "velos.json")
		tt.Ok(err)
		defer syscall.Unlink(f.Name())
		ioutil.WriteFile(f.Name(), []byte(bad), 0777)
		_, err = NewCfgFile(f.Name())
		tt.Assert(err != nil, "bad cfg accepted:[%s]", bad)
	}
}

func testServer(port int, auth *Auth) {
	s := &Server{ auth: auth }
	s.ListenAndServe(port)
}


func TestServer8080(t *testing.T) {
	testServer(8080, srv1)
}

func TestServer8081(t *testing.T) {
	testServer(8081, srv1)
}

func TestClientConnectionRefused(t *testing.T) {
	tt := base.Test{ T:t }
	// ...Missing server
	client := NewClient(cli1)
	result, err := client.Login()
	tt.Assert(err != nil, "error:%v", err)
	t.Logf("result: %v", result)
	t.Logf("error: %v", err)
}

func TestClientUnauthorized(t *testing.T) {
	tt := base.Test{ T:t }
	go testServer(8080, srv1)
	client := NewClient(cli2)
	result, err := client.startMeasuring(42, false)
	tt.Assert(err != nil, "error:%v", err)
	t.Logf("result: %v", result)
	t.Logf("error: %v", err)
}


func TestClientOk(t *testing.T) {
	tt := base.Test{ T:t }
	go testServer(8080, srv1)
	client := NewClient(cli1)
	log := func(result []string, err error) {
		t.Logf("%s%v%s\n", base.Green, result, base.Reset)
		tt.Ok(err)
	}
	// First login
	log(client.Login())

	log(client.startMeasuring(42, false))
	log(client.stopMeasuring(42, Unknown))

	log(client.startMeasuring(43, false))
	log(client.stopMeasuring(43, Manual))

	log(client.startMeasuring(44, false))
	log(client.startMeasuring(44, false))

	log(client.stopMeasuring(45, Unknown))

	log(client.startMeasuring(46, false))
	log(client.stopMeasuring(46, Unknown))

	log(client.startMeasuring(99, false))

	time.Sleep(2 * time.Second)
}

func TestClientMilk(t *testing.T) {

	tt := base.Test{ T:t }
	pwd, err := os.Getwd()
    tt.Ok(err)
    path := filepath.Join(pwd, "/../velos.json")
	cfg, err := NewCfgFile(path)
	if err != nil {
		t.Skip(err)
	}
	c := NewClient(cfg)
	log := func(result []string, err error) {
		if len(result) > 0 {
			fmt.Printf("path: %s%v%s\n", base.Green, result[0], base.Reset)
		}
		if len(result) > 1 {
			fmt.Printf("req:  %s%v%s\n", base.Green, result[1], base.Reset)
		}
		if len(result) > 2 {
			fmt.Printf("resp: %s%v%s\n", base.Green, result[2], base.Reset)
		}
		if err != nil {
			fmt.Printf("err:  %s%v%s\n", base.Red, err, base.Reset)
		}
	}

	c.Login() //milkings seems to be returned without session!!!
	fmt.Println("session:", c.session)

	log(c.postApi(Weighing, GetDailyAnimalWeightSummary, []any{ 1234, "5/19/2023 7:00:00 AM" }))
	hostname := []any{ map[string]any{ 
		"name":       "vpu_1", // seems is optional
		"ob_type":    "HostName",
		"ob_version": 1,
	}}

	// path: https://houston.vpu-online.com/api/devices
	// req:  {"jsonrpc":"2.0","id":2,"method":"get_logical_addresses","params":[{"name":"vpu_1","ob_type":"HostName","ob_version":1}]}
	// resp: {"jsonrpc":"2.0","id":2,"result":[{"daddress":0,"datype":"VP8002","ob_type":"DeviceAddressType","ob_version":1}]}
	log(c.postApi(Devices, GetLogicalAddresses, hostname))

	// path: https://houston.vpu-online.com/api/livestock
	// req:  {"jsonrpc":"2.0","id":3,"method":"get_animal","params":[1234]}
	// resp: {"jsonrpc":"2.0","id":3}
	log(c.postApi(Livestock, GetAnimal, []any{ 12345 })) // does not fail even when 12345 even does not exists







	// path: https://houston.vpu-online.com/api/milk
	// req:  {"jsonrpc":"2.0","id":1,"method":"clear","params":[]}
	// resp: {"jsonrpc":"2.0","id":1}
	log(c.postApi(Milk, Clear, []any{}))

	// path: https://houston.vpu-online.com/api/milk
	// req:  {"jsonrpc":"2.0","id":2,"method":"get_last_milking","params":[1234]}
	// resp: {"jsonrpc":"2.0","id":2}
	log(c.postApi(Milk, GetLastMilking, []any{ 1234 }))
	
	// path: https://houston.vpu-online.com/api/milk
	// req:  {"jsonrpc":"2.0","id":3,"method":"get_last_milking","params":["1234"]}
	// resp: {"jsonrpc":"2.0","id":3,"error":{"code":-32603,"message":"Current token (VALUE_STRING) not numeric, cannot use numeric value accessors\n at [Source: UNKNOWN; byte offset: #UNKNOWN]"}}
	// err:  500. response
	//log(c.postApi(Milk, GetLastMilking, []any{ "1234" }))
	
	// path: https://houston.vpu-online.com/api/milk
	// req:  {"jsonrpc":"2.0","id":3,"method":"get_last_milking","params":[]}
	// resp: {"jsonrpc":"2.0","id":3,"error":{"code":-32601,"message":"No such method"}}
	// err:  404. Not found: https://houston.vpu-online.com/api/milk
	//log(c.postApi(Milk, GetLastMilking, []any{ }))


	// path: https://houston.vpu-online.com/api/milk
	// req:  {"jsonrpc":"2.0","id":3,"method":"get_milk_data","params":[1234]}
	// resp: {"jsonrpc":"2.0","id":3}
	log(c.postApi(Milk, GetMilkData, []any{ 1234 }))
	
	// path: https://houston.vpu-online.com/api/milk
	// req:  {"jsonrpc":"2.0","id":4,"method":"get_milkings","params":["5/19/2023c 7:00:00 AM"]}
	// resp: {"jsonrpc":"2.0","id":4,"result":[]}
	log(c.postApi(Milk, GetMilkings, []any{ "5/19/2023 7:00:00 AM"})) // seems accepted any string


	// path: https://houston.vpu-online.com/api/milk
	// req:  {"jsonrpc":"2.0","id":6,"method":"get_milkings_for_animal","params":[1234]}
	// resp: {"jsonrpc":"2.0","id":6,"result":[]}
	log(c.postApi(Milk, GetMilkingsForAnimal, []any{ 1234 }))


	// path: https://houston.vpu-online.com/api/milk
	// req:  {"jsonrpc":"2.0","id":7,"method":"get_milkings_for_animal_since","params":[1234,"march"]}
	// resp: {"jsonrpc":"2.0","id":7,"result":[]}
	log(c.postApi(Milk, GetMilkingsForAnimalSince, []any{ 1234, "march" }))
}


func TestMeasuring(t *testing.T) {

	tt := base.Test{ T:t }
	pwd, err := os.Getwd()
    tt.Ok(err)
    path := filepath.Join(pwd, "/../velos.json") // out of repo reach
	cfg, err := NewCfgFile(path)
	if err != nil {
		t.Skip(err)
	}
	//tt.Ok(err)
	c := NewClient(cfg)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	debug := 2
	heartbeat := 5 * time.Second
	m := NewMeasuring(c, ctx, debug, heartbeat)
	t.Logf("Name: %s%s%s", base.Yellow, m.Name(), base.Reset)

	time.Sleep(1 * time.Second)
	m.Start(10, false)
	
	time.Sleep(1 * time.Second)
	m.Stop(10, Unknown)
	
	time.Sleep(1 * time.Second)
	var sb strings.Builder
	m.Health(&sb)
	t.Logf("Health: %s%s%s", base.Yellow, sb.String(), base.Reset)

}

func TestNewServer(t *testing.T) {
	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt)
	defer stop()
	NewServer(ctx, 8081, "u5er", "p455")
}
