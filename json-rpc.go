package velos

import (
	"encoding/json"
	"io"
)

// https://www.jsonrpc.org/specification

type Req struct {
	Jsonrpc string  `json:"jsonrpc"` // should be 2.0
	Id      float64 `json:"id"`
	Method  string  `json:"method"`
	Params  any     `json:"params"`
}

func NewJsonRpcReq(id int, method Method, params any) *Req {
	return &Req{
		Jsonrpc: "2.0",
		Id:      float64(id),
		Method:  string(method),
		Params:  params,  
	}
}

func (r *Req) Bytes() ([]byte, error) {
	return json.Marshal(r)
}

func (r *Req) String() string {
	b, _ := json.Marshal(r)
	return string(b)
}

type Resp struct {
	Jsonrpc string      `json:"jsonrpc"`          // should be 2.0
	Id      float64     `json:"id"`               // IMPORTANT don't set omitempty
	Result  interface{} `json:"result,omitempty"`
	Error   *Error      `json:"error,omitempty"`
}

func NewResp(body io.Reader) (*Resp, error) {
	var resp *Resp
	if err := json.NewDecoder(body).Decode(&resp); err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *Resp) String() string {
	b, _ := json.Marshal(r)
	return string(b)
}


type Error struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Data    any    `json:"data,omitempty"`
}

func (err *Error) resp(req *Req) *Resp {
	if req == nil {
		return &Resp{
			Jsonrpc: "2.0",
			Error:   err,
		}
	}
	return &Resp{
		Jsonrpc: req.Jsonrpc,
		Id:      req.Id,
		Error:   err,
	}
}

// Invalid JSON was received by the server.
// An error occurred on the server while parsing the JSON text.
func NewRespParseError() *Resp {
	return (&Error{
		Code:    -32700,
		Message: "parse error",
	}).resp(nil)
}

// The JSON sent is not a valid Request object.
func NewRespInvalidRequest() *Resp {
	return (&Error{
		Code:    -32600,
		Message: "invalid request",
	}).resp(nil)
}

// NewReq parses the body for a request.
// Returns request nil for two errors: parse-error and invalid-request
// Both errors responses are returned to be send to client
// Returns request no nil to be inspected for id,method,params.
func NewReq(body io.ReadCloser) (*Req, *Resp) {
	if body == nil {
		return nil, NewRespParseError()
	}
	defer body.Close()
	var req *Req
	if err := json.NewDecoder(body).Decode(&req); err != nil {
		return nil, NewRespInvalidRequest()
	}
	return req, nil
}

// The method does not exist / is not available.
func NewRespMethodNotFound(req *Req) *Resp {
	return (&Error{
		Code:    -32601,
		Message: "Method not implemented",
	}).resp(req)
}

// Invalid method parameter(s).
func NewRespInvalidParams(req *Req) *Resp {
	return (&Error {
		Code:    -32602,
		Message: "Invalid parameters",
	}).resp(req)
}

// Internal JSON-RPC error.
func NewRespInternalError(req *Req) *Resp {
	return (&Error {
		Code:    -32603,
		Message: "Internal error",
	}).resp(req)
}

func NewRespOk(req *Req, result any) *Resp {
	return &Resp{
		Jsonrpc: req.Jsonrpc,
		Id:      req.Id,
		Result:  result,
	}
}

func (r *Resp) Write(w io.Writer) (*Resp, error) {
	str, err := json.Marshal(&r)
	if err == nil {
		w.Write([]byte(str))
	}
	return r, err
}
