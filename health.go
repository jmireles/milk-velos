package velos

import (
	"fmt"
	"strings"
	"sync"

	"gitlab.com/jmireles/cans-base"
)

type Health struct {
	sync.Mutex

	name      string
	log       int

	starts    int64
	stops     int64
	errors    int64
	gets      int64

	lastPost  string
	lastReq   string
	lastResp  string
	lastError string
}

func NewHealth(name string, log int) *Health {
	return &Health{
		name: name,
		log:  log,
	}
}

func (h *Health) Start(result []string, err error) {
	h.Lock()
	defer h.Unlock()
	h.last(result, err)
	h.starts++
	if err != nil {
		h.errors++
	}
}

func (h *Health) Stop(result []string, err error) {
	h.Lock()
	defer h.Unlock()
	h.last(result, err)
	h.stops++
	if err != nil {
		h.errors++
	}
}

func (h *Health) Gets(result []string, err error) {
	h.Lock()
	defer h.Unlock()
	h.gets++
}

func (h *Health) Health(sb *strings.Builder) {
	h.Lock()
	defer h.Unlock()
	sb.WriteString(`{`)

	sb.WriteString(fmt.Sprintf(`"name":%q`, h.name))
	sb.WriteString(fmt.Sprintf(`,"gets":%d`, h.gets))
	sb.WriteString(fmt.Sprintf(`,"starts":%d`, h.starts))
	sb.WriteString(fmt.Sprintf(`,"stops":%d`, h.stops))
	sb.WriteString(fmt.Sprintf(`,"errors":%d`, h.errors))
	
	sb.WriteString(fmt.Sprintf(`,"last":{`))
	sb.WriteString(fmt.Sprintf(`"post":%q`, h.lastPost))
	sb.WriteString(fmt.Sprintf(`,"req":%q`, h.lastReq))
	sb.WriteString(fmt.Sprintf(`,"resp":%q`, h.lastResp))
	sb.WriteString(fmt.Sprintf(`,"error":%q`, h.lastError))
	sb.WriteString(`}`)
	
	sb.WriteString(`}`)
}

func (h *Health) last(result []string, err error) {
	if h.log > 1 {
		fmt.Printf("Velos\n")
	}
	if len(result) > 0 {
		if h.log > 1 {
			fmt.Printf("Post: %v\n", result[0])
		}
		h.lastPost = result[0]
	} else {
		h.lastPost = ""
	}
	if len(result) > 1 {
		if h.log > 1 {
			fmt.Printf("Req:  %s%v%s\n", base.Green, result[1], base.Reset)
		}
		h.lastReq = result[1]
	} else {
		h.lastReq = ""
	}
	if len(result) > 2 {
		if h.log > 1 {
			fmt.Printf("Resp: %s%v%s\n", base.Blue, result[2], base.Reset)
		}
		h.lastResp = result[2]
	} else {
		h.lastResp = ""
	}
	if err != nil {
		if h.log > 1 {
			fmt.Printf("Err:  %s%v%s\n", base.Red, err, base.Reset)
		}
		h.lastError = err.Error()
	} else {
		h.lastError = ""
	}
}
