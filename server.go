package velos

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"net/http/httptest"
	"time"

	"gitlab.com/jmireles/cans-base"
)

type Server struct {
	auth *Auth
}

func NewServer(ctx context.Context, port int, user, pass string) {
	s := &Server{
		auth: &Auth{
			user: user,
			pass: pass,
		},
	}
	ts := httptest.NewUnstartedServer(http.HandlerFunc(s.Handler))
	ts.Listener.Close()
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
	    panic(err)
	}
	ts.Listener = listener
	ts.Start()
	defer ts.Close()

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	for {
		select {
		case <-ctx.Done():
			return
		}
	}
}

func (s *Server) ListenAndServe(port int) error {
	http.HandleFunc("/", s.Handler)
    fmt.Printf("%sVelos start port: %d%s\n", base.Green, port, base.Reset)
    err := http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
    fmt.Printf("%sVelos stop err: %v%s\n", base.Red, err, base.Reset)
    return err
}

func (v *Server) Handler(w http.ResponseWriter, r *http.Request) {

//fmt.Println("server", r.URL.Path)

	switch r.Method {

	case "POST":
		switch r.URL.Path {
		case Session.String():
    		v.session(w, r)

    	case Smartflow.String():
    		v.smartflow(w, r)

    	case ProcessUnit.String():
    		v.processUnit(w, r)

    	default: 
			w.WriteHeader(404)
    	}

    default:
		w.WriteHeader(404)
	}
}


func (v *Server) session(w http.ResponseWriter, r *http.Request) {

	if req, err := v.rpcReq(w, r); err != nil {
		return
	} else {
		switch Method(req.Method) {

		case Login:
			if v.auth.LoginOk(w, r) {
				resp, _ := NewRespOk(req, 1).Write(w)
				v.log(r, req, resp)
			} else {
				v.log(r, req, nil)
			}

		case Logout:
			// TODO

		default:
			w.WriteHeader(200)
			resp, _ := NewRespMethodNotFound(req).Write(w)
			v.log(r, req, resp)
		}
	}
}

func (v *Server) smartflow(w http.ResponseWriter, r *http.Request) {

	if req, err := v.rpcReq(w, r); err != nil {
		return 
	} else if err := v.auth.CheckSession(w, r); err != nil {
		return
	} else {
		switch Method(req.Method) {

		case StartMeasuring:
			// TODO check req params
			resp, _ := NewRespOk(req, 1).Write(w)
			v.log(r, req, resp)
		
		case StopMeasuring:
			// TODO check req params
			resp, _ := NewRespOk(req, 1).Write(w)
			v.log(r, req, resp)

		default:
			resp, _ := NewRespMethodNotFound(req).Write(w)
			v.log(r, req, resp)
		}
	}	
}

func (v *Server) processUnit(w http.ResponseWriter, r *http.Request) {

	if req, err := v.rpcReq(w, r); err != nil {
		return
	} else if err := v.auth.CheckSession(w, r); err != nil {
		return
	} else {
		switch Method(req.Method) {

		case GetVersion:
			// TODO check req params
			resp, _ := NewRespOk(req, 1).Write(w)
			v.log(r, req, resp)
		}
	}
}

func (v *Server) rpcReq(w http.ResponseWriter, r *http.Request) (*Req, error) {

	w.Header().Set("content-type", "application/json-rpc")
	if req, errResp := NewReq(r.Body); errResp != nil {
		w.WriteHeader(200)
		errResp.Write(w)
		v.log(r, req, errResp)
		err := fmt.Errorf("Invalid RPC")
		fmt.Printf("Server: %s%s%s\n", base.Red, err, base.Reset)
		return nil, err
	} else {
		return req, nil
	}
}

func (s *Server) log(r *http.Request, req *Req, resp *Resp) {
	now := time.Now()
	fmt.Printf("Server: %s %s req:%s%v%s resp:%s%v%s\n", now.Format("15:04:05"), r.URL.Path, 
		base.Green, req, base.Reset, 
		base.Blue, resp, base.Reset,
	)
}

// VELOS go-Server was hacked from this velos #C app sends as StartMeasuring, StopMeasuring...
/*
using System;
using com.nedap.agri.services.milking;
using com.nedap.agri.services.smartflow;
using com.nedap.dweb.interfacing;
using com.nedap.dweb.util;
namespace smart {
    class Program {
    	private static VelosConnectionManager conn;
    	private static ISmartFlowService service;
        static void Main(string[] args) {
			CLogger.m_logLevel = LogLevel.LEVEL_INFO;
			conn = new VelosConnectionManager();
			conn.InitVpuConnection("http://192.168.1.70:8080", "userName", "password");
			service = new SmartFlowService();
			Console.WriteLine("Connection with Velos is established.");
            Console.WriteLine("Milk places 42-46 needs to exists but also place 99 don't");

            service.StartMeasuring(42);
            service.StopMeasuring(42);

            service.StartMeasuring(43);
            service.StopMeasuring(43, TakeOffReason.MANUAL);

            service.StartMeasuring(44);
            try {
                service.StartMeasuring(44);
            } catch (RpcException e) {
                Console.WriteLine($"RpcException double start {e}.");
            }
            try {
                service.StopMeasuring(45);
            } catch (RpcException e) {
                Console.WriteLine($"RpcException stop without start {e}.");
            }

            service.StartMeasuring(46);
            service.StopMeasuring(46);
            try {
                service.StartMeasuring(99);
            } catch (RpcException e) {
                Console.WriteLine($"RpcException invalid place {e}.");
            }
            Console.WriteLine("stars/stops sent.");
            conn.DisposeVpuConnection();
            Console.WriteLine("Connection with Velos is closed.");
            Environment.Exit(0);
        }
    }
}
*/


/*
C# request:
===========
conn = new VelosConnectionManager();
conn.InitVpuConnection("http://192.168.1.70:8080", "userName", "password");
service = new SmartFlowService();
service.StartMeasuring(42);
conn.DisposeVpuConnection();
Environment.Exit(0);

Server receptions:
==================
Server: /api/session req:{"jsonrpc":"2.0","id":0,"method":"login","params":null} resp: {"jsonrpc":"2.0","id":0,"result":1}
Server: /api/smartflow req:{"jsonrpc":"2.0","id":1,"method":"start_measuring","params":[42,false]} resp: {"jsonrpc":"2.0","id":1,"result":1}
Server: /api/processunit req:{"jsonrpc":"2.0","id":2,"method":"get_version","params":[{"name":"vpu_1","ob_type":"HostName","ob_version":1}]} resp: {"jsonrpc":"2.0","id":2,"result":1}


*/




