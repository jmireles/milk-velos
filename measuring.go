package velos

import (
	"context"
	"fmt"
	"strings"
	"time"
)

type Start struct {
	place    int
	reattach bool
}

type Stop struct {
	place  int
	reason TakeoffReason
}

type TakeoffReason string

const (
	Unknown   TakeoffReason = "UNKNOWN"
	Automatic TakeoffReason = "AUTOMATIC"
	Manual    TakeoffReason = "MANUAL"
	Reaching  TakeoffReason = "REACHING_ROTOR_POSITION"
)

type Measuring struct {

	start  chan *Start
	stop   chan *Stop
	name   string
	health *Health
}

func NewMeasuring(client *Client, ctx context.Context, debug int, heartbeat time.Duration) *Measuring {
	name   := fmt.Sprintf("Velos %s", client.cfg.URL)
	health := NewHealth(name, debug)
	start  := make(chan *Start)
	stop   := make(chan *Stop)
	go func() {
		ctx, cancel := context.WithCancel(ctx)
		defer cancel()
		ticker := time.NewTicker(heartbeat)
		defer ticker.Stop()
		for {
			select {
			case s := <- start:
				result, err := client.StartMeasuring(s)
				health.Start(result, err)

			case s := <- stop:
				result, err := client.StopMeasuring(s)
				health.Stop(result, err)

			case <-ticker.C:
				// heartbeat: prevents velos server 
				// stop accepting the session already granted.
				result, err := client.GetVersion()
				health.Gets(result, err)

			case <- ctx.Done():
				return
			}
		}
	}()
	return &Measuring{
		name:   name,
		health: health,
		start:  start,
		stop:   stop,
	}
}

func (m *Measuring) Name() string {
	return m.name
}

func (m *Measuring) Health(sb *strings.Builder) {
	m.health.Health(sb)
}

func (m *Measuring) Start(place int, reattach bool) {
	m.start<- &Start{
		place:    place,
		reattach: reattach,
	}
}

func (m *Measuring) Stop(place int, reason TakeoffReason) {
	m.stop<- &Stop{
		place:  place, 
		reason: reason,
	}
}


